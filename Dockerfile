FROM balenalib/raspberrypi4-64-alpine-golang:1.15-3.11-build as builder

WORKDIR /usr/src/app

ARG MOSQUITTO_EXPORTER_VERSION=0.8.0
RUN curl -Ls "https://github.com/sapcc/mosquitto-exporter/archive/refs/tags/v${MOSQUITTO_EXPORTER_VERSION}.tar.gz" \
  | tar -xvz -C .

RUN cd mosquitto-exporter-$MOSQUITTO_EXPORTER_VERSION \
    && go mod vendor \
    && go build -o ../mosquitto_exporter

FROM balenalib/raspberrypi4-64-alpine:3.11-run

COPY --from=builder /usr/src/app/mosquitto_exporter /usr/src/app/mosquitto_exporter

EXPOSE 9234

ENV BROKER_ENDPOINT=ws://mqtt-server:9001
ENV MQTT_CLIENT_ID=mosquitto_exporter

ENTRYPOINT [ "/usr/src/app/mosquitto_exporter" ]
